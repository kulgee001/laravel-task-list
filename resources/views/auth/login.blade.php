@extends('layouts.app')

@section('content')



<div class="row">

<div class="col-md-4 col-md-offset-4">

@include('common.errors')

<form class="form-signin" action="/auth/login" method="POST">
{{ csrf_field() }}
  <h2 class="form-signin-heading">Please sign in</h2>
  <label for="email" class="sr-only">Email address</label>
    <input type="email" id="email" name="email" class="form-control" placeholder="Email address" required autofocus><br>
  <label for="password" class="sr-only">Password</label>
    <input type="password" id="password" name="password" class="form-control" placeholder="Password" required>
  <div class="checkbox">
    <label>
      <input type="checkbox" value="remember-me" name="remember"> Remember me
    </label>
  </div>
  <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button><br>
  <a class="btn btn-lg btn-danger btn-block" href="/auth/register">Register Here!</a>
</form>

</div>

</div>

@endsection