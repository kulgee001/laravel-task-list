@extends('layouts.app')

@section('content')

<div class="row">

<div class="col-md-4 col-md-offset-4">

@include('common.errors')

<form action="/auth/register" method="POST">
{{ csrf_field() }}
  <div class="form-group">
    <label for="name">Fullname</label>
    <input type="text" class="form-control" name="name" id="name" placeholder="Fullname">
  </div>
  <div class="form-group">
    <label for="email">Email address</label>
    <input type="email" class="form-control" name="email" id="email" placeholder="Email">
  </div>
  <div class="form-group">
    <label for="password">Password</label>
    <input type="password" class="form-control" name="password" id="password" placeholder="Password">
  </div>
  <div class="form-group">
    <label for="repassword">Retype Password</label>
    <input type="password" class="form-control" name="password_confirmation" id="repassword" placeholder="Retype Password">
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>

</div>

</div>

@endsection